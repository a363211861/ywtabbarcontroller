//
//  YWTabBar.h
//  YWTabBarController
//
//  Created by Y W on 13-7-29.
//  Copyright (c) 2013年 Y W. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YWTabBarItem.h"

#define YWTabBarHeight 49.0f

@protocol YWTabBarDelegate;


@interface YWTabBar : UIImageView

@property (nonatomic, weak) id<YWTabBarDelegate> delegate;
@property (nonatomic, readonly) YWTabBarItem *selectedItem;

- (void)setTabBarItems:(NSArray *)items;

- (void)setSelectItem:(YWTabBarItem *)selectItem;

@end



@protocol YWTabBarDelegate <NSObject>

@optional
- (BOOL)tabBar:(YWTabBar *)tabBar shouldSelectItem:(YWTabBarItem *)item;
- (void)tabBar:(YWTabBar *)tabBar didSelectItem:(YWTabBarItem *)item;

@end