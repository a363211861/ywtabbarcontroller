//
//  AppDelegate.h
//  YWTabBarController
//
//  Created by Y W on 13-7-29.
//  Copyright (c) 2013年 Y W. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
