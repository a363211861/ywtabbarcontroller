//
//  YWTabBarController.h
//  YWTabBarController
//
//  Created by Y W on 13-7-29.
//  Copyright (c) 2013年 Y W. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "YWTabBar.h"

@protocol YWTabBarControllerDelegate;

@interface YWTabBarController : UIViewController <YWTabBarDelegate>

- (id)initWithTabBarItems:(NSArray *)items;

@property (nonatomic, weak) id<YWTabBarControllerDelegate> delegate;

@property (nonatomic, readonly) YWTabBar *tabBar;
@property (nonatomic, readonly) YWTabBarItem *selectedItem;
@property (nonatomic, assign) int selectIndex;

@end


@protocol  YWTabBarControllerDelegate <NSObject>

@optional

- (BOOL)tabBarController:(YWTabBarController *)tabBarController shouldSelectTabBarItem:(YWTabBarItem *)tabBarItem;

- (void)tabBarController:(YWTabBarController *)tabBarController didSelectTabBarItem:(YWTabBarItem *)tabBarItem;

@end