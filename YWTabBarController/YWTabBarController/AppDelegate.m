//
//  AppDelegate.m
//  YWTabBarController
//
//  Created by Y W on 13-7-29.
//  Copyright (c) 2013年 Y W. All rights reserved.
//

#import "AppDelegate.h"
#import "YWTabBarController.h"
#import "ViewController.h"
#import "TableViewController.h"

@implementation AppDelegate

- (void)dealloc
{
    [_window release];
    [super dealloc];
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    NSMutableArray *items = [[NSMutableArray alloc] init];
    {
        ViewController *viewController = [[ViewController alloc] init];
        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:viewController];
        viewController.title = @"1";
        [viewController release];
        
        YWTabBarItem *item = [[YWTabBarItem alloc] init];
        item.title = @"1";
        item.normalImage = [UIImage imageNamed:@"1.png"];
        item.selectedImage = [UIImage imageNamed:@"1.png"];
        item.heighlightImage = [UIImage imageNamed:@"1.png"];
        item.viewController = navigationController;
        [navigationController release];
        
        [items addObject:item];
        [item release];
    }
    
    {
        ViewController *viewController = [[ViewController alloc] init];
        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:viewController];
        viewController.title = @"2";
        [viewController release];
        
        YWTabBarItem *item = [[YWTabBarItem alloc] init];
        item.title = @"2";
        item.normalImage = [UIImage imageNamed:@"2.png"];
        item.selectedImage = [UIImage imageNamed:@"2.png"];
        item.heighlightImage = [UIImage imageNamed:@"2.png"];
        item.viewController = navigationController;
        [navigationController release];
        
        [items addObject:item];
        [item release];
    }
    
    {
        TableViewController *viewController = [[TableViewController alloc] initWithStyle:UITableViewStylePlain];
        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:viewController];
        viewController.title = @"3";
        [viewController release];
        
        YWTabBarItem *item = [[YWTabBarItem alloc] init];
        item.title = @"3";
        item.normalImage = [UIImage imageNamed:@"3.png"];
        item.selectedImage = [UIImage imageNamed:@"3.png"];
        item.heighlightImage = [UIImage imageNamed:@"3.png"];
        item.viewController = navigationController;
        [navigationController release];
        
        [items addObject:item];
        [item release];
    }
    
    
    self.window = [[[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]] autorelease];
    // Override point for customization after application launch.
    self.window.backgroundColor = [UIColor whiteColor];
    self.window.rootViewController = [[[YWTabBarController alloc] initWithTabBarItems:items] autorelease];
    [items release];
    [self.window makeKeyAndVisible];
    [[(YWTabBarController *)self.window.rootViewController tabBar] setImage:[UIImage imageNamed:@"tabbarbg.png"]];
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
