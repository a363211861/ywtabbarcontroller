//
//  YWTabBar.m
//  YWTabBarController
//
//  Created by Y W on 13-7-29.
//  Copyright (c) 2013年 Y W. All rights reserved.
//

#import "YWTabBar.h"

#define kButtonTag 2390

@interface YWTabBar ()

@property (nonatomic, strong) NSArray *tabBarItems;
@property (nonatomic, weak) UIButton *selectButton;

@end

@implementation YWTabBar

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.userInteractionEnabled = YES;
    }
    return self;
}

- (YWTabBarItem *)selectedItem
{
    if (self.selectButton = nil) {
        return nil;
    }
    return [self.tabBarItems objectAtIndex:self.selectButton.tag - kButtonTag];
}

- (void)setTabBarItems:(NSArray *)items
{
    _tabBarItems = items;
    
    self.selectButton = nil;
    
    if (items.count == 0) {
        return;
    }
    
    [self.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    CGFloat width = ceilf(self.bounds.size.width / items.count);
    for (int i = 0; i < items.count; i++) {
        
        YWTabBarItem *item = [items objectAtIndex:i];
        
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.tag = kButtonTag + i;
        button.frame = CGRectMake(width * i, 0, width, self.frame.size.height);
        
        [button setImage:item.normalImage forState:UIControlStateNormal];
        [button setImage:item.selectedImage forState:UIControlStateSelected];
        [button setImage:item.heighlightImage forState:UIControlStateHighlighted];
        
        [button setTitle:item.title forState:UIControlStateNormal];
        [button setTitleColor:[UIColor colorWithRed:130/255.0 green:127/255.0 blue:127/255.0 alpha:1] forState:UIControlStateNormal];
        [button setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
        [button setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
        [button.titleLabel setFont:[UIFont boldSystemFontOfSize:12]];
        [button setContentVerticalAlignment:UIControlContentVerticalAlignmentTop];
        [button setTitleEdgeInsets:UIEdgeInsetsMake(floor(self.frame.size.height-17), -floor(width), 0, 0)];
        [button setContentVerticalAlignment:UIControlContentVerticalAlignmentBottom];
        [button setContentHorizontalAlignment:UIControlContentHorizontalAlignmentCenter];
        
        [button addTarget:self action:@selector(tabBarButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:button];
    }
}


- (void)setSelectItem:(YWTabBarItem *)selectItem
{
    int index = [self.tabBarItems indexOfObject:selectItem];
    if (index == NSNotFound || self.tabBarItems == nil) {
        return;
    }
    
    UIButton *button = (UIButton *)[self viewWithTag:kButtonTag + index];
    [self tabBarButtonClicked:button];
}


- (void)tabBarButtonClicked:(UIButton *)button
{
    YWTabBarItem *item = [self.tabBarItems objectAtIndex:button.tag - kButtonTag];
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(tabBar:shouldSelectItem:)]) {
        if (![self.delegate tabBar:self shouldSelectItem:item]) {
            return;
        }
    }
    
    if (self.selectButton != nil) {
        self.selectButton.selected = NO;
    }
    self.selectButton = button;
    self.selectButton.selected = YES;
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(tabBar:didSelectItem:)]) {
        [self.delegate tabBar:self didSelectItem:item];
    }
}

@end
