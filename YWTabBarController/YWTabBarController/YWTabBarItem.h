//
//  YWTabBarItem.h
//  YWTabBarController
//
//  Created by Y W on 13-7-29.
//  Copyright (c) 2013年 Y W. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YWTabBarItem : NSObject

@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) UIImage *normalImage;
@property (nonatomic, strong) UIImage *heighlightImage;
@property (nonatomic, strong) UIImage *selectedImage;
@property (nonatomic, strong) UIViewController *viewController;

@end
