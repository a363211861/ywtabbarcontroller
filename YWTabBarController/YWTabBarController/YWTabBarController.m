//
//  YWTabBarController.m
//  YWTabBarController
//
//  Created by Y W on 13-7-29.
//  Copyright (c) 2013年 Y W. All rights reserved.
//

#import "YWTabBarController.h"

@interface YWTabBarController () <UINavigationControllerDelegate>

@property (nonatomic, strong) UIView *contentView;
@property (nonatomic, strong) NSArray *tabBarItems;

@end

@implementation YWTabBarController

- (id)initWithTabBarItems:(NSArray *)items
{
    self = [super init];
    if (self) {
        self.tabBarItems = items;
        
        self.delegate = nil;
        _tabBar = nil;
        self.selectIndex = -1;
        _selectedItem = nil;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.hidesBottomBarWhenPushed = NO;
    
    {
        for (YWTabBarItem *item in self.tabBarItems) {
            if ([item.viewController isKindOfClass:[UINavigationController class]]) {
                UINavigationController *navController = (UINavigationController *)item.viewController;
                navController.delegate = self;
                UINavigationBar *navBar = [navController navigationBar];
                CGRect rect = navBar.frame;
                rect.origin = CGPointZero;
                navBar.frame = rect;
            }
            [self addChildViewController:item.viewController];
        }
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    if (self.contentView == nil) {
        
        UIView *mView = [[UIView alloc] init];
        mView.backgroundColor = [UIColor clearColor];
        mView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
        [self.view addSubview:mView];
        
        self.contentView = mView;
    }
    
    if (_tabBar == nil) {
        YWTabBar *tabBar = [[YWTabBar alloc] initWithFrame:CGRectMake(0, self.view.bounds.size.height - YWTabBarHeight, self.view.bounds.size.width, YWTabBarHeight)];
        tabBar.delegate = self;
        [self.view addSubview:tabBar];
        
        [tabBar setTabBarItems:self.tabBarItems];
        _tabBar = tabBar;
    }
    
    if (_selectedItem) {
        if (_selectedItem.viewController.hidesBottomBarWhenPushed) {
            [self contentViewFullScreenFrame];
            [self tabBarFullScreenFrame];
        } else {
            [self contentViewNotFullScreenFrame];
            [self tabBarNotFullScreenFrame];
        }
    } else {
        [self contentViewNotFullScreenFrame];
        [self tabBarNotFullScreenFrame];
        [self.tabBar setSelectItem:[self.tabBarItems objectAtIndex:0]];
    }
}

#pragma mark - frames
- (void)contentViewFullScreenFrame
{
    self.contentView.frame = self.view.bounds;
}

- (void)tabBarFullScreenFrame
{
    self.tabBar.frame = CGRectMake(0, self.view.bounds.size.height, self.view.bounds.size.width, YWTabBarHeight);
}


- (void)contentViewNotFullScreenFrame
{
    self.contentView.frame = CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height - YWTabBarHeight);
}

- (void)tabBarNotFullScreenFrame
{
    self.tabBar.frame = CGRectMake(0, self.view.bounds.size.height - YWTabBarHeight, self.view.bounds.size.width, YWTabBarHeight);
}


#pragma mark - YWTabBarDelegate
- (BOOL)tabBar:(YWTabBar *)tabBar shouldSelectItem:(YWTabBarItem *)item
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(tabBarController:shouldSelectTabBarItem:)]) {
        return [self.delegate tabBarController:self shouldSelectTabBarItem:item];
    }
    
    return YES;
}

- (void)tabBar:(YWTabBar *)tabBar didSelectItem:(YWTabBarItem *)item
{
    if (_selectedItem) {
        if (_selectedItem == item) {
            if ([_selectedItem.viewController respondsToSelector:@selector(popViewControllerAnimated:)]) {
                [(UINavigationController *)_selectedItem.viewController popViewControllerAnimated:YES];
            }
        } else {
            [self transitionFromViewController:_selectedItem.viewController toViewController:item.viewController duration:0.05 options:UIViewAnimationOptionTransitionCrossDissolve animations:^ {
                
            } completion:^(BOOL finished) {
                _selectedItem = item;
            }];
        }
    } else {
        _selectedItem = item;
        [self.contentView addSubview:item.viewController.view];
    }
}

#pragma mark - UINavigationControllerDelegate

- (void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated
{
    if (viewController.hidesBottomBarWhenPushed) { //需要隐藏tabBar
        if (!self.hidesBottomBarWhenPushed) { //当前tabBar是显示的
            [self contentViewFullScreenFrame];
            [UIView animateWithDuration:0.25 animations:^{
                [self tabBarFullScreenFrame];
            }];
            self.hidesBottomBarWhenPushed = YES;
        }
    } else { //需要显示tabBar
        if (self.hidesBottomBarWhenPushed) {
            [self contentViewNotFullScreenFrame];
            [UIView animateWithDuration:0.25 animations:^{
                [self tabBarNotFullScreenFrame];
            }];
            self.hidesBottomBarWhenPushed = NO;
        }
    }
}

- (void)navigationController:(UINavigationController *)navigationController didShowViewController:(UIViewController *)viewController animated:(BOOL)animated
{
    
}

@end
